clear;
close all;
clc;

w = rand(2, 1);
w = 2 * w - 1;
miu = 0.1;
teta = -0.3;
epoch = 100;

input_points = [0 1 2 0 1 2 3 3 4 4;-3 -3 -3 -2 -2 1 1 0 1 0];
target = [1 1 1 1 1 0 0 0 0 0];
result = [0 0 0 0 0 0 0 0 0 0];

plotpv(input_points,target)
hold on;

while epoch > 0 && sum(result) ~= 10
    disp('------------------------------------------------')
    for j = 1:10
        net = w(1)*input_points(1, j) + w(2)*input_points(2,j);
        if net < teta
            out = 0;
        else
            out = 1;
        end
          
%         out = sgn(w.*input_points(:, j), teta);
        if target(j) - out == 0
            result(j) = true;
%           continue
        elseif target(j) - out == 1
            result(j) = false;
            w = w + miu * input_points(:, j);
        elseif target(j) - out == -1
            result(j) = false;
            w = w - miu * input_points(:, j);
        end
%     x = [input_points(1,j) input_points(2,j)];
    x = -1:6;
    percept_line = (-w(1)/w(2))*x + teta/w(2);
    plot(x, percept_line, 'r')
    pause(0.1)
    disp(['point:', num2str(input_points(1,j)),' ',num2str(input_points(2,j)),'    ',  'out:', num2str(out), '    ', 'target: ', num2str(target(j))])
    end
    epoch = epoch - 1;
end
disp('task Done')
disp(['w1: ', num2str(w(1)), '    ', 'w2: ', num2str(w(2))])
