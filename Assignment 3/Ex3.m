clear;
close all;
clc;

w = rand(3, 1); % w(3) == teta
w = 2 * w - 1;
eta = 0.3;
epoch = 7;
error_threshold = 0.01;

input_points = [0 1 2 0 1 2 3 3 4 4;-3 -3 -3 -2 -2 1 1 0 1 0;-1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
% input_points = [0 1 2 0 1 2 3 3 4 4;-3 -3 -3 -2 -2 1 1 0 1 0]; % for scale
% scaled_inputs = rescale(input_points);
% scaled_inputs(3,:) = [-1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
target = [1 1 1 1 1 0 0 0 0 0];

plotpv(input_points(1:2,:),target) 
hold on;


previous_error = 0;
while epoch > 0
    disp('------------------------------------------------')
    current_error = 0;
    for j = randperm(10)
        net = w(1)*input_points(1, j) + w(2)*input_points(2,j) - w(3);
        out = 1/(1+exp(-net));
        w = w + eta * (target(j) - out)*out*(1 - out) * input_points(:, j);
        
        current_error = current_error + 1/2 * (target(j) - out)^2 ;

        x = -1:6;
        percept_line = (-w(1)/w(2))*x + (w(3)/w(2));
        plot(x, percept_line, 'g')
        pause(0.1)
        disp(['point:', num2str(input_points(1,j)),' ',num2str(input_points(2,j)),'    ',  'out:', num2str(out), '    ', 'target: ', num2str(target(j))])
    end

    if abs(previous_error - current_error) < error_threshold
        disp(['Error difference :', num2str(previous_error - current_error)])
        break
    end
    previous_error = current_error;
    epoch = epoch - 1;
end
plot(x, percept_line, 'r', 'LineWidth',2)
hold on
plotpv(input_points(1:2,:),target) 
disp('task Done')
disp(['w1: ', num2str(w(1)), '    ', 'w2: ', num2str(w(2)), '     teta: ', num2str(w(3))])

