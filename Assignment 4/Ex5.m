clear
close all
clc


x=[0 0 1 1;0 1 0 1]; % Input
d=[0 1 1 0]; % Target

[~,p] = size(x);

% x1=[x;ones(1,p)];
x1=[x;-1 -1 -1 -1];
w1 = rand(2,3);
w2 = rand(1,3);
eta = 0.7;
it = 1000;


for i=1:it
    fprintf('Inputs:\n')
    disp(x1(1:2,:))
    fprintf('Iteration:%i \n',i)
    error_sum = 0;
    dw1=0;
    dw2=0;
    for j=randperm(p)
        net1 = w1*x1(:,j);
        out = 1./(1 + exp(-net1));
%         out1 = [out;-1];
        out1 = [out;-1];
        net2 = w2*out1;
        out2 = 1./(1 + exp(-net2));

        error_sum = error_sum + 1/2*((d(j) - out2)^2);

        delta2 = (d(j)-out2)*out2*(1-out2);
        delta1_1 = (delta2*w2(1))*out(1)*(1-out(1));
        delta1_2 = (delta2*w2(2))*out(2)*(1-out(2));
 
        dw2_new = eta*delta2*out1;
        dw2 = dw2_new + dw2;
        w2 = w2 + dw2';

        dw1_1_new = eta*delta1_1*(x1(:,j));
        dw1_2_new = eta*delta1_2*(x1(:,j));
        dw1 = dw1 + [dw1_1_new,dw1_2_new];
%         dw1 = [dw1_1_new,dw1_2_new];
        w1 = w1 + dw1';
    end
    fprintf('error in last layer: %i', error_sum)
%     plotfun(w1, x, d, p)
    ploterror(error_sum, i)
    if error_sum<=50e-3
        break
    end
    clc
end
fprintf('Converged after %i iteration \n',i)
fprintf('Training Finished! \n')

figure
plotfun(w1, x, d, p)
% ezplot([num2str(w1(1,1)),'*x+',num2str(w1(1,2)),'*y+',num2str(w1(1,3))],[-1 2]);
% ezplot([num2str(w1(2,1)),'*x+',num2str(w1(2,2)),'*y+',num2str(w1(2,3))],[-1 2]);
xP = -1:2 ;
ypercept_line1 = (-w1(1,1)/(w1(1,2)))*xP+(w1(1,3)/w1(1,2));
ypercept_line2 = (-w1(2,1)/(w1(2,2)))*xP+(w1(2,3)/w1(2,2));
plot(xP,ypercept_line1, 'r');
plot(xP,ypercept_line2, 'r');
drawnow


figure
hold on
view(3)
points=0:0.04:1;
for x=points
    for y=points
        net_1_ = w1*[x;y;-1];
        sigmoid_1 = 1 ./ (1 + exp(-net_1_));
        net_2_ = -w2*[sigmoid_1;-1];
        sigmoid_2 = 1 ./ (1 + exp(net_2_));
        plot3(x,y,sigmoid_2,'.')
    end
end


function ploterror(err, epoch)
xlim([0 epoch])
ylim([0 1])
xlabel('epoch');
ylabel('error');
hold on
plot(epoch,err,'o')
drawnow
end


function plotfun(w, x, d, p)
hold on
grid on
for i=1:p
    if d(i)==0
        plot(x(1,i),x(2,i),'o','Markerfacecolor',[0 0 1])
    else
        plot(x(1,i),x(2,i),'o','Markerfacecolor',[1 0 0])
    end
end
axis equal
xlim([-1 2])
ylim([-1 2])
%     line1 = ezplot([num2str(w(1,1)),'*x+',num2str(w(1,2)),'*y+',num2str(w(1,3))],[-1 2]);
%     line2 = ezplot([num2str(w(2,1)),'*x+',num2str(w(2,2)),'*y+',num2str(w(2,3))],[-1 2]);
xP = -1:2 ;
ypercept_line1 = (-w(1,1)/(w(1,2)))*xP+(w(1,3)/w(1,2));
ypercept_line2 = (-w(2,1)/(w(2,2)))*xP+(w(2,3)/w(2,2));
h11 = plot(xP,ypercept_line1,'r');
h22 = plot(xP,ypercept_line2, 'r');
drawnow
delete(h11)
delete(h22)
end