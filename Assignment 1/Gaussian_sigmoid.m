x = -5:0.01:5;
sigmoid = 1./(1+exp(-x));

subplot(1,2,1);
plot(x, sigmoid)
grid on

subplot(1,2,2); 
sigma = 1.5;
miu = 0.001;
gaussian = (1./sigma.*sqrt(2*pi)).* exp((-1/2)*((x-miu)./sigma).^2);
plot(x, gaussian)
grid on
