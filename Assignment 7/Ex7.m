close all;
clear;
clc;

% input args
n = input('Please enter n: ');
m = input('Please enter m: ');
a_max = input('Please enter a_max: ');
a_min = input('Please enter a_min: ');

% load dataset
data = readmatrix('iris.dat');
data = data(:,1:2)/100;
[data_row_size, data_col_size] = size(data);
t_max = data_row_size;

% plot data points
f = figure;
f.WindowState = "maximized";
hold on
scatter(data(:,1), data(:,2), 'filled', 'black')

% declare neurons & setting random weights
neuron = zeros(n,m,data_col_size);
for ii=1:n
    for jj=1:m
        neuron(ii,jj,:) = [(0.8 - 0.4) * rand(1) + 0.4,(0.5 - 0.1) * rand(1) + 0.1];
    end
end

for t=1:data_row_size

    % find winner neurons
    min = 1;
    for i=1:n
        for j=1:m
            temp(i,j) = norm([neuron(i,j,1),neuron(i,j,2)] - data(t,:));
            if temp(i,j) < min
                min = temp(i,j);
                index_of_min = [i, j];
            end
        end
    end
    
    eta = (a_max - a_min) * ((t_max - t)/(t_max - 1)) + a_min;
    for i=1:n
        for j=1:m
            distnace = pdist([i,j;index_of_min], "cityblock");
            sigma = 2*exp(-t/data_row_size);
            d = exp(-(distnace^2)/(2*(sigma^2)));
            neuron(i,j,:) = [neuron(i,j,1),neuron(i,j,2)] + eta * d * (data(t,:) - [neuron(i,j,1),neuron(i,j,2)]);
        end
    end
    network_plot(neuron,n,m,t,data_row_size)
end


function network_plot(neuron,n,m,t,data_row_size)
    s = scatter(neuron(:,:,1), neuron(:,:,2),"red", "filled");
    index =1;
    for i=1:n
        for j=1:m

            for p=i:n
                for q=1:m
                    d = pdist([i,j;p,q]);

                    % find neighbors
                    if d==1
                        pl(index) = plot([neuron(i,j,1);neuron(p,q,1)],[neuron(i,j,2);neuron(p,q,2)],'blue');
                        index = index + 1;
                    end

                end
            end

        end
    end

    drawnow
    if t~=data_row_size
        delete(s)
        delete(pl)
    end
end

