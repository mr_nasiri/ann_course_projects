clc;clear;close all

% Load iris dataset
data = readmatrix('iris.dat');

% Split data to training and test set
[m,n] = size(data);
p = 0.7;
idx_rand = randperm(m);
training_set = data(idx_rand(1:round(p*m)),:);
[trow, tcol] = size(training_set);

test_set = data(idx_rand(round(p*m)+1:end), :);
[test_row, test_col] = size(test_set);

% Decide the position of centers using K-mean
number_of_centers = input("Enter number of centers: ");
[idx,centers] = kmeans(training_set(:,1:4), number_of_centers);

% Calculate spread
Spread = input("Enter spread: ");
% max = 0;
% for ii=1:number_of_centers-1
%     for jj=ii+1:number_of_centers
%         distance = sqrt((centers(ii, 1)-centers(jj,1))^2 + (centers(ii, 2)-centers(jj,2))^2 + (centers(ii, 3)-centers(jj,3))^2 + (centers(ii, 4)-centers(jj,4))^2);
%         if distance > max
%             max = distance;
%         end
%     end
% end
% Spread = max / sqrt(2*number_of_centers);

% define phi matrix
phi = zeros(trow, number_of_centers);

% calculate phi matrix
for i = 1:trow
    for j = 1:number_of_centers
        phi(i,j) = exp((-norm(training_set(i,1:4) - centers(j,:)))/2*(Spread^2));
    end
end

% calculate Weights
w = inv(transpose(phi)*phi)*transpose(phi)*training_set(:,5);

% calculate output and add bias
out = phi * w;
out = round(out);
% out = out - ones(1);


% validation test
% calculate phi matrix
phi_test = zeros(test_row, number_of_centers);
for i = 1:test_row
    for j = 1:number_of_centers
        phi_test(i,j) = exp((-norm(test_set(i,1:4) - centers(j,:)))/2*(Spread^2));
    end
end

out_test = phi_test * w;

% result on test set : suggest Lable , target, is_correct
result = [round(out_test), test_set(:, 5), round(out_test) == test_set(:, 5)]

% accuracy on test set
accuracy = sum((result(:, 3)/test_row)*100)







