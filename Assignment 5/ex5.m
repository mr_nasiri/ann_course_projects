close all
clear
clc

x=-6:0.05:6;
t=sin(x)+x.*cos(3*x);

nourons=15;
net = fitnet(nourons);
net.trainParam.epochs=1000;
net.Divideparam.trainRatio=0.6;
net.Divideparam.valRatio=0.2;
net.Divideparam.testRatio=0.2;
net = train(net,x,t);

rng=-6:0.01:6;
y2=net(rng);

plot(x,t,'Ok');
hold on
plot(rng,y2,'m', LineWidth=2);